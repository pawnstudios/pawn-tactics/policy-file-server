/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame.config;

public class Config {
    public static int SERVER_PORT = 843;
    public static String POLICY_FILE_XML =
        "<cross-domain-policy>" +
            "<site-control permitted-cross-domain-policies=\"master-only\" />" +
            "<allow-access-from domain=\"*\" to-ports=\"9339\" />" +
        "</cross-domain-policy>";
}
