/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame.handler;

import com.pawngame.config.Config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PolicyFileRequestHandler {
    private byte[] response;

    private static PolicyFileRequestHandler instance = new PolicyFileRequestHandler();

    public static PolicyFileRequestHandler getInstance() {
        return instance;
    }

    private PolicyFileRequestHandler() {
        try {
            byte[] bytes = Config.POLICY_FILE_XML.getBytes();
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            output.write(bytes);
            output.write((byte) 0);
            response = output.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] generateResponse() {
        return response.clone();
    }
}
