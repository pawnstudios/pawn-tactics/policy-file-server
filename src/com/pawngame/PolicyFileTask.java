/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame;

import com.pawngame.handler.PolicyFileRequestHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.ByteBuffer;

public class PolicyFileTask implements Runnable {
    private Socket socket;

    private static ByteBuffer requestInfo = ByteBuffer.wrap("<policy-file-request/>".getBytes());

    private int index = 0;

    public PolicyFileTask(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run()  {
        try {
            InputStream inputStream = socket.getInputStream();
            int input;
            while((input = inputStream.read()) != -1) {
                if ((byte) input ==  requestInfo.get(index)) {
                    index++;
                    if (index == requestInfo.limit()) {
                        socket.getOutputStream().write(PolicyFileRequestHandler.getInstance().generateResponse());
                        socket.getOutputStream().flush();
                        socket.close();
                        break;
                    }
                } else {
                    socket.close();
                    break;
                }
            }
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
