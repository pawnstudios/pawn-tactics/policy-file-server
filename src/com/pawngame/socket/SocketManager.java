/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame.socket;

import com.pawngame.CloseSocketTask;
import com.pawngame.PolicyFileTask;
import com.pawngame.config.Config;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

public class SocketManager {
    ThreadPoolExecutor executor = new ThreadPoolExecutor(5, 200, 10, TimeUnit.MINUTES, new LinkedBlockingDeque<>());
    ScheduledExecutorService closeScheduler = new ScheduledThreadPoolExecutor(5);
    ServerSocket socket;

    public void listen() throws IOException {
        socket = new ServerSocket();
        InetSocketAddress address = new InetSocketAddress("0.0.0.0", Config.SERVER_PORT);
        socket.bind(address);
    }

    public void startAccepting() throws IOException {
        while (true) {
            Socket job = socket.accept();
            executor.execute(new PolicyFileTask(job));
            closeScheduler.schedule(new CloseSocketTask(job), 3, TimeUnit.SECONDS);
        }
    }
}

