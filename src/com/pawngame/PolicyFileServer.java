/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame;

import com.pawngame.socket.SocketManager;

import java.io.IOException;

public class PolicyFileServer {
    private final SocketManager socketManager;

    public PolicyFileServer() {
        socketManager = new SocketManager();
    }

    public void start() throws IOException {
        socketManager.listen();
        socketManager.startAccepting();
    }
}
