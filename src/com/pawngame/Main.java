/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame;

import com.pawngame.socket.SocketManager;

public class Main {
    private static PolicyFileServer policyFileServer;

    public static void main(String[] args) throws Exception {
        policyFileServer = new PolicyFileServer();
        policyFileServer.start();
    }
}
