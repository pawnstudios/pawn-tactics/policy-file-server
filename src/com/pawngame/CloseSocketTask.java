/*
 * Copyright � 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.pawngame;

import java.io.IOException;
import java.net.Socket;

public class CloseSocketTask implements Runnable {

    private Socket socket;

    public CloseSocketTask(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            socket.close();
        } catch (IOException e) {}
    }
}
